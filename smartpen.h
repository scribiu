/*
 * scribiu -- read notebooks and voice memos from Livescribe pens
 * Copyright (C) 2015 Javier S. Pedro <javier@javispedro.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SMARTPEN_H
#define SMARTPEN_H

#include <QtCore/QObject>
#include <QtCore/QDateTime>
#include <QtCore/QVariantMap>
#include <openobex/obex.h>

#define SMARTPEN_DPI_X (677.3333)
#define SMARTPEN_DPI_Y (677.3333)

// TODO: These values are obtained by observation and may be wrong
#define SMARTPEN_BLEED_X 333.3
#define SMARTPEN_BLEED_Y 333.3

class Smartpen : public QObject
{
	Q_OBJECT

public:
	explicit Smartpen(QObject *parent = 0);
	~Smartpen();

	typedef QPair<unsigned int, unsigned int> Address;
	typedef quint64 PenId;
	typedef quint64 PenTime;

	bool isConnected() const;

	enum class Parameter : quint16 {
		Id = 0x0000,
		/// The offset between the RtcTime (see below) and the user's configured time. This value is fixed at setup time.
		UserTime = 0x8003,
		/// The current time as reported by the pen's rtc
		RtcTime = 0x8004,
		Type = 0x8006,
		Name = 0x8011
	};

	QByteArray getObject(const QString& name);
	QByteArray getParameter(Parameter parameter);

	PenId getPenId();
	QString getPenSerial();
	QString getPenName();
	PenTime getPenTime(Parameter parameter);

	QVariantMap getPenInfo();

	struct ChangeReport {
		QString guid;
		QString className;
		QString title;
		PenTime endTime;
	};

	QList<ChangeReport> getChangeList(PenTime from = 0);

	QByteArray getLspData(const QString &name, PenTime from = 0);
	QByteArray getPaperReplay(PenTime from = 0);

	static QDateTime fromPenTime(PenTime userTime, PenTime penTime);

	static QString toPenSerial(PenId id);
	static PenId toPenId(const QString &serial);

	static bool reset(const Address &addr);

public slots:
	bool connectToPen(const Address &addr);
	void disconnectFromPen();

signals:
	void linkError(const QString &msg);

private:
	static void obexEventCb(obex_t *handle, obex_object_t *obj,
	                            int mode, int event, int obex_cmd, int obex_rsp);
	void handleObexEvent(obex_object_t *object,
	                     int event, int obex_cmd, int obex_rsp);
	void handleObexRequestDone(obex_object_t *object, int obex_cmd, int obex_rsp);
	void handleObexContinue(obex_object_t *object, int obex_cmd);

	void prepareRequest();
	bool waitForRequestComplete(int timeout);
	void addConnHeader(obex_object_t *object) const;
	bool sendContinue(int obex_cmd);

	static QString toPenSerialSegment(quint32 id, int len);
	static quint32 fromPenSerialSegment(const QString &s);

	static QByteArray encodeUtf16(const QString &s);


private:
	obex_t * _obex;
	quint32 _connId;
	QByteArray _inBuf;
	bool _reqComplete;
	uint _continueReceived;
};

#endif // SMARTPEN_H
