/*
 * scribiu -- read notebooks and voice memos from Livescribe pens
 * Copyright (C) 2015 Javier S. Pedro <javier@javispedro.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtCore/QDebug>
#include <QtGui/QPen>
#include <QtWidgets/QGraphicsSceneMouseEvent>
#include "notebookview.h"
#include "pageitem.h"
#include "stfgraphicsitem.h"

PageItem::PageItem(AfdNotebook *nb, PaperReplay *replay, int pageNum, QGraphicsItem *parent) :
    QGraphicsItem(parent), _nb(nb), _replay(replay), _pageNum(pageNum), _strokesLoaded(false)
{
	_pageSize = _nb->getPageSize(pageNum);
	_pageTrim = _nb->getPageTrim(pageNum);

	QGraphicsRectItem *border = new QGraphicsRectItem(_pageTrim, this);
	border->setPen(QPen(Qt::black));

	QPixmap bgPix = _nb->getPageBackground(_pageNum);
	QGraphicsPixmapItem *bg = new QGraphicsPixmapItem(bgPix, this);
	bg->setShapeMode(QGraphicsPixmapItem::BoundingRectShape);
	bg->setTransformationMode(Qt::SmoothTransformation);
	QRectF bgRect = bg->boundingRect();
	bg->setScale(std::min(_pageTrim.width() / bgRect.width(), _pageTrim.height() / bgRect.height()));
	bg->setPos(_pageTrim.topLeft());
}

QRectF PageItem::boundingRect() const
{
	return QRectF(QPointF(0, 0), _pageSize);
}

void PageItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	Q_UNUSED(painter);
	Q_UNUSED(option);
	Q_UNUSED(widget);
	if (!_strokesLoaded) {
		createStrokes();
		_strokesLoaded = true;
	}
}

int PageItem::type() const
{
	return Type;
}

int PageItem::pageNum() const
{
	return _pageNum;
}

void PageItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
	QGraphicsView *view = scene()->views().first();
	if (NotebookView *nbview = qobject_cast<NotebookView*>(view)) {
		nbview->focusOnPage(_pageNum);
		event->accept();
		return;
	}
	QGraphicsItem::mouseDoubleClickEvent(event);
}

void PageItem::createStrokes()
{
	QStringList pens = _nb->penSerials();
	if (pens.isEmpty()) return;

	// Load paper replay data if available
	AfdPageAddress pageAddr = _nb->getPageAddress(_pageNum);
	PaperReplay::SessionList replays = _replay ? _replay->sessions(pageAddr.toUInt64()) : PaperReplay::SessionList();

	// Now create a new StfGraphicsItem for every stroke file
	// Which will in turn create a stroke item for every stroke
	QStringList strokeFiles = _nb->strokeFiles(pens.first(), _pageNum);
	foreach (const QString &strokeFile, strokeFiles) {
		QFile f(strokeFile);
		if (!f.open(QIODevice::ReadOnly)) {
			qWarning() << "Could not open stroke file:" << strokeFile;
			continue;
		}
		new StfGraphicsItem(&f, replays, this);
	}

	qDebug() << "strokes loaded for page" << _pageNum;
}
