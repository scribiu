/*
 * scribiu -- read notebooks and voice memos from Livescribe pens
 * Copyright (C) 2015 Javier S. Pedro <javier@javispedro.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "smartpen.h"
#include "notebookmodel.h"
#include "paperreplaymodel.h"

PaperReplayModel::PaperReplayModel(PaperReplay *replay, QObject *parent) :
    QAbstractTableModel(parent), _replay(replay)
{
}

QVariant PaperReplayModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid()) return QVariant();
	int row = index.row();
	int column = index.column();
	const PaperReplay::Session &session = _sessions.at(row);
	switch (column) {
	case 0:
		switch (role) {
		case Qt::DisplayRole:
			return getSessionName(session);
		}
		break;
	case 1:
		switch (role) {
		case Qt::DisplayRole:
			return getSessionLength(session);
		}
		break;
	}

	return QVariant();
}

QVariant PaperReplayModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	switch (orientation) {
	case Qt::Horizontal:
		switch (section) {
		case 0:
			switch (role) {
			case Qt::DisplayRole:
				return tr("Recording");
			}
			break;
		case 1:
			switch (role) {
			case Qt::DisplayRole:
				return tr("Length");
			}
			break;
		}
		break;
	case Qt::Vertical:
		break;
	}
	return QVariant();
}

int PaperReplayModel::rowCount(const QModelIndex &parent) const
{
	if (parent.isValid()) return 0;
	return _sessions.size();
}

int PaperReplayModel::columnCount(const QModelIndex &parent) const
{
	if (parent.isValid()) return 0;
	return 2;
}

QString PaperReplayModel::sessionFilename(const QModelIndex &index) const
{
	if (!index.isValid()) return QString();
	int row = index.row();
	const PaperReplay::Session &session = _sessions.at(row);
	return session.fileName();
}

void PaperReplayModel::refresh()
{
	beginResetModel();
	_sessions = _replay->sessions();
	std::sort(_sessions.begin(), _sessions.end(), PaperReplay::Session::compareByStartTime);
	endResetModel();
}

QString PaperReplayModel::getSessionName(const PaperReplay::Session &session) const
{
	QString title = session.name();

	if (title.isEmpty()) {
		QDateTime date = Smartpen::fromPenTime(_replay->userTime(), session.startTime());
		title = date.toString(Qt::DefaultLocaleLongDate);
	}

	return title;
}

QString PaperReplayModel::getSessionLength(const PaperReplay::Session &session) const
{
	quint64 msecs = session.duration();
	uint secs = msecs / 1000;
	uint mins = secs / 60;
	secs %= 60;
	uint hours = mins / 60;
	mins %= 60;

	const QChar fill('0');
	if (hours) {
		return QString("%1:%2:%3").arg(hours).arg(mins, 2, 10, fill).arg(secs, 2, 10, fill);
	} else {
		return QString("%2:%3").arg(mins).arg(secs, 2, 10, fill);
	}
}
