/*
 * scribiu -- read notebooks and voice memos from Livescribe pens
 * Copyright (C) 2015 Javier S. Pedro <javier@javispedro.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AFDPAGEADDRESS_H
#define AFDPAGEADDRESS_H

#include <QtCore/QString>

class AfdPageAddress
{
public:
	AfdPageAddress();
	AfdPageAddress(uint section, uint segment, uint shelf, uint book, uint page);
	AfdPageAddress(uint segment, uint shelf, uint book, uint page);
	explicit AfdPageAddress(const QString &addr);
	explicit AfdPageAddress(quint64 addr);

	uint section() const;
	uint segment() const;
	uint shelf() const;
	uint book() const;
	uint page() const;

	QString toString() const;
	quint64 toUInt64() const;

private:
	quint64 v;
};

inline AfdPageAddress::AfdPageAddress() : v(0)
{
}

inline AfdPageAddress::AfdPageAddress(quint64 addr) : v(addr)
{
}

inline uint AfdPageAddress::section() const
{
	return (v >> 52) & 0xFFFU;
}

inline uint AfdPageAddress::segment() const
{
	return (v >> 40) & 0xFFFU;
}

inline uint AfdPageAddress::shelf() const
{
	return (v >> 24) & 0xFFFFU;
}

inline uint AfdPageAddress::book() const
{
	return (v >> 12) & 0xFFFU;
}

inline uint AfdPageAddress::page() const
{
	return (v >> 0) & 0xFFFU;
}

inline quint64 AfdPageAddress::toUInt64() const
{
	return v;
}

#endif // AFDPAGEADDRESS_H
