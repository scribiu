# Screenshots

![Screenshot](http://depot.javispedro.com/livescribe/scribiu-tutorial.png)

# About

**Scribiu** is a program that shows the notebooks and voice notes (_paper replay_)
from [Livescribe Echo](http://www.livescribe.com) smartpens.
It also allows you to export individual pages as PNG files or voice memos as AAC files.

# Requirements

Scribiu requires Qt 5, including the core, gui, widgets, svg and multimedia modules.
It also requires libudev, openobex (>=1.7.2), libusb (>=1.0) and QuaZip (1.0).
Most of these should be packaged by your GNU/Linux distribution.

For example, on Ubuntu, these correspond with packages:

`
qtbase5-dev
libqt5svg5-dev
qtmultimedia5-dev
libqt5multimedia5-plugins
libudev-dev
libopenobex2-dev
libusb-1.0-0-dev
libquazip5-dev
`

This program should work with the Livescribe Pulse as well as the Livescribe Echo.
It does not work with the Livescribe 3, Aegir, Symphony or any of the newer Bluetooth pens.
It may work with the Livescribe Sky if you get it to work with Echo Desktop, but I have never tried.

# Install

`qmake`, `make`, and (`sudo`) `make install` should be enough.

A udev rule will be installed in `/lib/udev/rules.d/60-livescribe.rules` that will take care of the proper permissions when a Smartpen is detected.
You may need to reboot or reload the udev daemon in order for these changes to work.

![Scribiu icon](http://depot.javispedro.com/livescribe/scribiu-icon-48.png)
Look for the Scribiu icon inside the Office menu.

It should automatically start synchronizing after connecting a Smartpen.

# Design

By default Scribiu stores its information inside `$XDG_DATA_HOME/scribiu`.
Inside there you will find a directory for every synchronized pen, and inside the pen directory, you will find a subdirectory for each one of your LiveScribe notebooks.
These directories contain the raw notebook, stroke, and voice data as received from the pen -- no processing is done.

Therefore, even if Scribiu fails to display a certain notebook, you may be able to use other Livescribe programs in order to view the synchronized data.

Many thanks to the authors of [libsmartpen](https://github.com/srwalter/libsmartpen)
and [LibreScribe](https://github.com/dylanmtaylor/LibreScribe) for their efforts
reverse engineering the protocols, specially regarding the STF data format,
which has saved me a lot of time.

# Tasks

* Notebook archiving support: it should just be a matter of cp-ing the .afd file
  into a new .pen directory. However, need to take care of paper replay and possibly
  generate a new GUID for the archived notebook.
* Paper replay stroke animation.
* Export entire notebook as PDF.
* Deleting stuff from the pen: for now it's best to do this from Echo Desktop.
