/*
 * scribiu -- read notebooks and voice memos from Livescribe pens
 * Copyright (C) 2015 Javier S. Pedro <javier@javispedro.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STFREADER_H
#define STFREADER_H

#include <QtCore/QPoint>
#include "bitreader.h"

class StfReader
{
public:
	class StrokeHandler
	{
	public:
		virtual ~StrokeHandler();
		virtual bool startStroke(const QPoint& p, int force, qint64 time) = 0;
		virtual bool strokePoint(const QPoint& p, int force, qint64 time) = 0;
		virtual bool endStroke(qint64 time) = 0;
	};

protected:
	struct CodeTable {
		int bits;
		int size;
		qint8 * data;
	};

	StrokeHandler *handler;
	int speed;

	bool parseV1(BitReader& br);
	void syncV1(BitReader& br);
	qint8 decodeV1(BitReader& br, CodeTable* tab, int tab_size);

	qint8 readForce(BitReader& br);
	qint8 readHeader(BitReader& br);
	qint8 readHeader2(BitReader& br);
	qint8 readHeader3(BitReader& br);
	qint8 readTime(BitReader& br);
	qint8 readDeltaX(BitReader& br);
	qint8 readDeltaY(BitReader& br);
	qint8 readDeltaF(BitReader& br);

public:
    StfReader();
	~StfReader();

	bool parse(QIODevice *device);
	bool parse(const QString &filename);
	void setStrokeHandler(StrokeHandler *newHandler);
};

#endif // STFREADER_H
