/*
 * scribiu -- read notebooks and voice memos from Livescribe pens
 * Copyright (C) 2015 Javier S. Pedro <javier@javispedro.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bitreader.h"

namespace
{
inline qint64 signExtend(quint64 x, int n)
{
	if (n < 64 && (x & (1 << (n-1)))) {
		x |= -(1LL << n);
	}
	return x;
}
}

BitReader::BitReader(QIODevice *device)
	: child(device), buf(0), avail(0)
{
}

BitReader::~BitReader()
{
}

quint64 BitReader::readBits(int n)
{
	quint64 x = peekBits(n);

	Q_ASSERT(avail >= n);
	buf -= x << (avail - n);
	avail -= n;

	return x;
}

qint64 BitReader::readSignedBits(int n)
{
	quint64 x = readBits(n);
	return signExtend(x, n);
}

quint64 BitReader::peekBits(int n)
{
	while (n > avail) {
		char c;
		child->getChar(&c);
		buf = (buf << 8) | (quint8)(c);
		avail += 8;
	}
	quint64 x = buf >> (avail - n);

	return x;
}

qint64 BitReader::peekSignedBits(int n)
{
	quint64 x = peekBits(n);
	return signExtend(x, n);
}

void BitReader::skipUntilNextByte()
{
	int skip = avail % 8;
	readBits(skip);
}

bool BitReader::atEnd()
{
	return avail == 0 && child->atEnd();
}
