/*
 * scribiu -- read notebooks and voice memos from Livescribe pens
 * Copyright (C) 2015 Javier S. Pedro <javier@javispedro.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NOTEBOOKVIEW_H
#define NOTEBOOKVIEW_H

#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGraphicsItem>
#include "afdnotebook.h"
#include "paperreplay.h"
#include "pageitem.h"

class NotebookView : public QGraphicsView
{
	Q_OBJECT
	Q_PROPERTY(QString notebook WRITE setNotebook READ notebook)
	Q_PROPERTY(QString paperReplay WRITE setPaperReplay READ paperReplay)
	Q_PROPERTY(QList<int> pageNumbers READ pageNumbers NOTIFY pageNumbersChanged)
	Q_PROPERTY(int curPage READ curPage WRITE setCurPage NOTIFY curPageChanged)
	Q_PROPERTY(int zoom READ zoom WRITE setZoom NOTIFY zoomChanged)

public:
	explicit NotebookView(QWidget *parent = 0);

	void setNotebook(const QString &path);
	QString notebook() const;

	void setPaperReplay(const QString &path);
	QString paperReplay() const;

	void setPenUserTime(quint64 userTime);
	quint64 penUserTime() const;

	QList<int> pageNumbers() const;

	int curPage() const;
	void setCurPage(int page);

	int zoom() const;

	QSize getCurPageSize() const;
	QRect getCurPageTrim() const;

	QImage exportPageAsImage(int pageNum) const;
	void renderPage(QPainter *painter, int pageNum, const QRectF &target = QRectF(), const QRectF &source = QRectF()) const;

	void exportPageAsTXYP(QIODevice *device, int pageNum, bool relativeTime);
	void exportPageAsInkML(QIODevice *device, int pageNum);

	void requestPaperReplay(const QString &file, qint64 time);

signals:
	void pageNumbersChanged();
	void curPageChanged();
	void zoomChanged(int zoom);
	void paperReplayRequested(const QString &file, qint64 time);

public slots:
	void clear();
	void setZoom(int zoom);
	void prevPage();
	void nextPage();
	void focusOnPage(int pageNum);

protected:
	void resizeEvent(QResizeEvent *event);
	void scrollContentsBy(int dx, int dy);

private:
	void removePages();
	bool createPages();
	void calculateScale();
	void layoutPages();

private:
	AfdNotebook *_nb;
	PaperReplay *_replay;
	QString _nbPath;
	QString _replayPath;
	quint64 _penUserTime;
	QMap<int, PageItem*> _pages;
	QSizeF _maxPageSize;
	int _numColumns;
	int _zoom;
	int _curPage;
};

#endif // NOTEBOOKVIEW_H
