TARGET = scribiu
VERSION = 1.5.3
TEMPLATE = app

QT += core gui widgets svg multimedia

CONFIG += c++11

CONFIG += link_pkgconfig
PKGCONFIG += libudev libusb-1.0 openobex

packagesExist(quazip1-qt5) {
    PKGCONFIG += quazip1-qt5
} else {
    # Some "distributions" forgot to distribute the pkg-config file...
    LIBS += -lquazip5
    INCLUDEPATH += /usr/include/quazip5
}

SOURCES += main.cc \
    mainwindow.cc \
    smartpenmanager.cc \
    notebookmodel.cc \
    smartpensyncer.cc \
    smartpen.cc bitreader.cc stfreader.cc \
    stfexporter.cc \
    xmlutils.cc \
    notebookview.cc \
    afdnotebook.cc \
    pageitem.cc \
    stfgraphicsitem.cc \
    paperreplay.cc \
    afdpageaddress.cc \
    stfstrokeitem.cc \
    paperreplaymodel.cc

HEADERS  += mainwindow.h \
    smartpenmanager.h \
    notebookmodel.h \
    smartpensyncer.h \
    smartpen.h bitreader.h stfreader.h \
    stfexporter.h \
    xmlutils.h \
    notebookview.h \
    afdnotebook.h \
    pageitem.h \
    stfgraphicsitem.h \
    paperreplay.h \
    afdpageaddress.h \
    stfstrokeitem.h \
    paperreplaymodel.h

FORMS    += mainwindow.ui

isEmpty(PREFIX) {
	PREFIX = /usr
}

isEmpty(UDEVDIR) {
        UDEVDIR = $$system(pkg-config --variable=udevdir udev)
}

target.path = $$PREFIX/bin

udev.path = $$UDEVDIR/rules.d
udev.files = 60-livescribe.rules

icon_scalable.path = $$PREFIX/share/icons/hicolor/scalable/apps
icon_scalable.files = icons/scalable/scribiu.svg

icon_48.path = $$PREFIX/share/icons/hicolor/48x48/apps
icon_48.files = icons/48x48/scribiu.png

desktop.path = $$PREFIX/share/applications
desktop.files = scribiu.desktop

INSTALLS += target udev icon_scalable icon_48 desktop

OTHER_FILES = icons/inkscape/scribiu.svg README.md COPYING
