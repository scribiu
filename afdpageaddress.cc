/*
 * scribiu -- read notebooks and voice memos from Livescribe pens
 * Copyright (C) 2015 Javier S. Pedro <javier@javispedro.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtCore/QDebug>
#include <QtCore/QStringList>
#include "afdpageaddress.h"

namespace {
quint64 addressToUint64(uint section, uint segment, uint shelf, uint book, uint page)
{
	return quint64(section & 0xFFFU) << 52 |
	       quint64(segment & 0xFFFU) << 40 | quint64(shelf & 0xFFFFU) << 24 |
	       quint64(book & 0xFFFU) << 12 | quint64(page & 0xFFFU);
}
}

AfdPageAddress::AfdPageAddress(uint section, uint segment, uint shelf, uint book, uint page)
    : v(addressToUint64(section, segment, shelf, book, page))
{
}

AfdPageAddress::AfdPageAddress(uint segment, uint shelf, uint book, uint page)
    : v(addressToUint64(0, segment, shelf, book, page))
{
}

AfdPageAddress::AfdPageAddress(const QString &addr)
{
	uint section = 0, segment = 0, shelf = 0, book = 0, page = 0;
	QStringList parts = addr.split('.');
	if (parts.count() == 5) {
		section = parts[0].toUInt();
		segment = parts[1].toUInt();
		shelf = parts[2].toUInt();
		book = parts[3].toUInt();
		page = parts[4].toUInt();
	} else if (parts.count() == 4) {
		section = 0;
		segment = parts[0].toUInt();
		shelf = parts[1].toUInt();
		book = parts[2].toUInt();
		page = parts[3].toUInt();
	} else {
		qWarning() << "Unknown page address syntax:" << addr;
	}
	v = addressToUint64(section, segment, shelf, book, page);
}

QString AfdPageAddress::toString() const
{
	QStringList l;
	l.reserve(5);

	if (section()) {
		l.append(QString::number(section()));
	}

	l.append(QString::number(segment()));
	l.append(QString::number(shelf()));
	l.append(QString::number(book()));
	l.append(QString::number(page()));

	return l.join(".");
}
