/*
 * scribiu -- read notebooks and voice memos from Livescribe pens
 * Copyright (C) 2015 Javier S. Pedro <javier@javispedro.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAPERREPLAY_H
#define PAPERREPLAY_H

#include <QtCore/QDir>
#include <QtCore/QHash>
#include <QtCore/QMap>
#include <QtCore/QVector>

/** Name of the paper replay "fake" notebook.
 *  Contains all paper replay sessions not tied to another notebook. */
#define PAPER_REPLAY "Paper Replay"
#define PAPER_REPLAY_GUID 0

class PaperReplay : public QObject
{
	Q_OBJECT

public:
	typedef quint64 SessionId;
	typedef quint64 PenTime;
	typedef quint64 PageAddress;

private:
	struct SessionData : public QSharedData {
		SessionId id;
		QString name;
		PenTime start, end;
		QVector<PageAddress> pages;
		QString file;
	};

public:
	explicit PaperReplay(QObject *parent = 0);

	class Session {
	public:
		Session();
		~Session();

		bool isValid() const;

		SessionId id() const;

		QString name() const;

		PenTime startTime() const;
		PenTime endTime() const;
		PenTime duration() const;

		QVector<quint64> pages() const;

		QString fileName() const;

		static bool compareByStartTime(const Session &a, const Session &b);

	private:
		Session(SessionId id);
		QSharedDataPointer<SessionData> d;

		friend class PaperReplay;
		friend class SessionList;
	};

	class SessionList {
	public:
		SessionList();

		QList<Session> sessionsDuringTime(PenTime time) const;

	private:
		explicit SessionList(const QMap<PenTime, Session>& byTime);

		QMap<PenTime, Session> _m;

		friend class PaperReplay;
	};

	bool open(const QString &path, quint64 notebookGuid, PenTime userTime);
	void close();

	QList<Session> sessions() const;
	SessionList sessions(PageAddress pageAddress) const;

	PenTime userTime() const;

private:
	bool parseSessionInfo(SessionData *session, const QString &path) const;
	bool parseSessionInfo(SessionData *session, QIODevice *dev) const;
	bool parseSessionInfoV3(SessionData *session, QIODevice *dev) const;

	bool parseSessionPages(SessionData *session, const QString &path) const;
	bool parseSessionPages(SessionData *session, QIODevice *dev) const;
	bool parseSessionPagesV1(SessionData *session, QIODevice *dev) const;

private:
	QDir _dir;
	QHash<PageAddress, Session> _sessions;
	QMap<PageAddress, QMap<PenTime, Session> > _byPageTime;
	PenTime _userTime;
};

#endif // PAPERREPLAY_H
