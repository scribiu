/*
 * scribiu -- read notebooks and voice memos from Livescribe pens
 * Copyright (C) 2015 Javier S. Pedro <javier@javispedro.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QMainWindow>
#include <QtWidgets/QLabel>
#include <QtMultimedia/QMediaPlayer>
#include "notebookmodel.h"
#include "paperreplaymodel.h"
#include "smartpenmanager.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

public slots:
	void closeNotebook();
	void openNotebook(const QString &pen, const QString &notebook);

	void exportCurrentPageAsPng(const QString &file);
	void exportCurrentPageAsSvg(const QString &file);
	void exportCurrentPageAsTXYP(const QString &file, bool relativeTime);
	void exportCurrentPageAsInkML(const QString &file);
	void exportCurrentPaperReplayAsAac(const QString &file);

private slots:
	void handleNotebookSelected(const QModelIndex &index);
	void handleNotebookRowsInserted(const QModelIndex &index, int start, int end);
	void handleCurPageChanged();
	void handlePaperReplaySelected(const QModelIndex &index);
	void handlePaperReplayRequested(const QString &file, qint64 time);
	void handlePaperReplayPlay();
	void handlePaperReplayPause();
	void handlePaperReplaySliderChanged(int value);
	void handlePlayerStateChanged(QMediaPlayer::State state);
	void handlePlayerDurationChanged(qint64 time);
	void handlePlayerPositionChanged(qint64 time);
	void handlePlayerSeekableChanged(bool seekable);
	void handlePensBeingSynchronizedChanged();
	void handlePenSyncComplete(const QString &penName);
	void handlePenSyncFailed(const QString &penName);
	void handleExport();
	void handleAbout();

protected:
	void closeEvent(QCloseEvent *event);

private:
	QString formatDuration(qint64 time) const;
	QString currentPlayerMediaPath() const;

private:
	Ui::MainWindow *ui;
	NotebookModel *_notebooks;
	SmartpenManager *_manager;

	QMediaPlayer *_player;
	qint64 _pendingSeek;

	QString _curPenName;
	QString _curNotebookName;

	PaperReplay *_replay;
	PaperReplayModel *_replayModel;

	QLabel *_statusLabel;
};

#endif // MAINWINDOW_H
