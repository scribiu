/*
 * scribiu -- read notebooks and voice memos from Livescribe pens
 * Copyright (C) 2015 Javier S. Pedro <javier@javispedro.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtCore/QDebug>
#include <QtGui/QPainterPath>

#include "stfreader.h"
#include "smartpen.h"
#include "stfstrokeitem.h"
#include "stfgraphicsitem.h"

class StfToGraphicsPathItems : public StfReader::StrokeHandler {
	QGraphicsItem *parent;
	PaperReplay::SessionList replays;
	PaperReplay::Session replay;
	qint64 startTime;
	QPainterPath path;
	QRectF bound;

public:
	StfToGraphicsPathItems(const PaperReplay::SessionList &replays, QGraphicsItem* parent)
		: parent(parent), replays(replays), path(), bound(0.0, 0.0, 1.0, 1.0) {
	}

	bool startStroke(const QPoint& p, int force, qint64 time) {
		Q_UNUSED(force);
		QList<PaperReplay::Session> sessions = replays.sessionsDuringTime(time);
		if (!sessions.isEmpty()) {
			replay = sessions.first();
		} else {
			replay = PaperReplay::Session();
		}
		startTime = time;
		path = QPainterPath(QPointF(p));
		return true;
	}

	bool strokePoint(const QPoint& p, int force, qint64 time) {
		Q_UNUSED(force);
		Q_UNUSED(time);
		path.lineTo(QPointF(p));
		return true;
	}

	bool endStroke(qint64 time) {
		bound |= path.boundingRect();
		new StfStrokeItem(path, replay, startTime, time, parent);
		/* Parent will take the child down with him when deleted. */
		return true;
	}

	const QRectF& boundingRect() {
		return bound;
	}
};

StfGraphicsItem::StfGraphicsItem(QIODevice *dev, const PaperReplay::SessionList &replays, QGraphicsItem *parent) :
	QGraphicsItem(parent)
{
	setFlags(ItemHasNoContents);

	StfToGraphicsPathItems h(replays, this);
	StfReader r;
	r.setStrokeHandler(&h);
	if (r.parse(dev)) {
		bbox = h.boundingRect();
	}
}

QRectF StfGraphicsItem::boundingRect() const
{
	return bbox;
}

void StfGraphicsItem::paint(QPainter *, const QStyleOptionGraphicsItem *, QWidget *)
{
	/* Intentionally empty; ItemHasNoContents; children will do the painting. */
}

int StfGraphicsItem::type() const
{
	return Type;
}
