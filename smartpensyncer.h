/*
 * scribiu -- read notebooks and voice memos from Livescribe pens
 * Copyright (C) 2015 Javier S. Pedro <javier@javispedro.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SMARTPENSYNCER_H
#define SMARTPENSYNCER_H

#include <atomic>
#include <QtCore/QThread>
#include <QtCore/QDir>
#include "smartpen.h"

class SmartpenSyncer : public QThread
{
	Q_OBJECT

public:
	explicit SmartpenSyncer(const Smartpen::Address &addr, QObject *parent = 0);
	~SmartpenSyncer();

	Smartpen::Address penAddress() const;
	bool hasErrors() const;

signals:
	void gotPenName(const QString &name);

public slots:
	void abort();
	void reset();

private:
	void run();
	bool syncPen();
	bool syncNotebook(Smartpen::PenTime lastSync, const Smartpen::ChangeReport &change);
	bool syncPaperReplay(Smartpen::PenTime lastSync, const Smartpen::ChangeReport &change);
	bool extractZip(QByteArray &zipData, QDir &dir);

private slots:
	void handleLinkError(const QString &msg);

private:
	const Smartpen::Address _addr;
	std::atomic<bool> _errored;
	std::atomic<bool> _aborted;

	// To be used only from this thread
	Smartpen *_pen;
	QString _penName;
	QDir _penDataDir;
};

#endif // SMARTPENSYNCER_H
