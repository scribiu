/*
 * scribiu -- read notebooks and voice memos from Livescribe pens
 * Copyright (C) 2015 Javier S. Pedro <javier@javispedro.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SMARTPENMANAGER_H
#define SMARTPENMANAGER_H

#include <QtCore/QMap>
#include <QtCore/QObject>
#include <QtCore/QPair>
#include <QtCore/QSocketNotifier>
#include <QtCore/QTimer>
#include "smartpensyncer.h"

struct udev;
struct udev_monitor;
struct udev_device;

class SmartpenManager : public QObject
{
	Q_OBJECT

public:
	explicit SmartpenManager(QObject *parent = 0);
	~SmartpenManager();

	QStringList pensConnected() const;
	QStringList pensBeingSynchronized() const;

signals:
	void syncComplete(const QString &penName);
	void syncFailed(const QString &penName);
	void pensConnectedChanged();
	void pensBeingSynchronizedChanged();

private slots:
	void handleMonitorActivity();
	void handleSyncerFinished();
	void handleGotPenName(const QString &name);
	void handleTimerNextTry();

private:
	void processDeviceAdded(udev_device *dev);
	void processDeviceRemoved(udev_device *dev);

	void trySync(const Smartpen::Address &addr);
	void scheduleNextTry();

	static QString parseUdevEscapedString(const char *s);
	static Smartpen::Address getDeviceAddress(udev_device *dev);

private:
	udev *_udev;
	udev_monitor *_monitor;
	QSocketNotifier *_notifier;

	struct PenInfo {
		Smartpen::Address addr;
		bool connected = false;
		QString name;
		SmartpenSyncer *syncer = 0;
		QDateTime nextTry;
		uint numRetries = 0;
	};

	QMap<Smartpen::Address, PenInfo> _pens;
	QTimer *_nextTry;
};

#endif // SMARTPENMANAGER_H
