/*
 * scribiu -- read notebooks and voice memos from Livescribe pens
 * Copyright (C) 2015 Javier S. Pedro <javier@javispedro.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAGEITEM_H
#define PAGEITEM_H

#include <QtWidgets/QGraphicsItem>

#include "afdnotebook.h"
#include "paperreplay.h"

class PageItem : public QGraphicsItem
{
public:
	explicit PageItem(AfdNotebook *nb, PaperReplay *replay, int pageNum, QGraphicsItem *parent = 0);

	enum { Type = UserType + 'p' };

	QRectF boundingRect() const;
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
	int type() const;

	int pageNum() const;

protected:
	void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) override;

private:
	void createStrokes();

private:
	AfdNotebook *_nb;
	PaperReplay *_replay;
	int _pageNum;
	QSize _pageSize;
	QRect _pageTrim;
	bool _strokesLoaded;
};

#endif // PAGEITEM_H
