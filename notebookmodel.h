/*
 * scribiu -- read notebooks and voice memos from Livescribe pens
 * Copyright (C) 2015 Javier S. Pedro <javier@javispedro.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NOTEBOOKMODEL_H
#define NOTEBOOKMODEL_H

#include <QtCore/QAbstractItemModel>
#include <QtCore/QDir>
#include <QtCore/QFileSystemWatcher>
#include <QtGui/QIcon>

#define PEN_EXTENSION "pen"
#define ARCHIVE_EXTENSION "archive"
#define AFD_NOTEBOOK_EXTENSION "afd"

#define PEN_USER_TIME_FILE ".usertime"
#define PEN_LAST_SYNC_FILE ".lastsync"
#define PEN_SYNC_LOCK_FILE ".sync.lck"

class NotebookModel : public QAbstractItemModel
{
	Q_OBJECT

public:
	explicit NotebookModel(QObject *parent = 0);

	typedef quint64 PenTime;

	enum Roles {
		FileNameRole = Qt::UserRole
	};

	static QString defaultDataDirectory();
	static QString userDataDirectory();

	QString penDirectory(const QString &name) const;
	PenTime penUserTime(const QString &name) const;
	QString notebookDirectory(const QString &penName, const QString &nbName) const;
	QString notebookDirectory(const QModelIndex &index) const;
	QString paperReplayDirectory(const QString &name) const;

	QVariant data(const QModelIndex &index, int role) const;
	Qt::ItemFlags flags(const QModelIndex &index) const;
	QModelIndex index(int row, int column, const QModelIndex &parent) const;
	QModelIndex parent(const QModelIndex &child) const;
	int rowCount(const QModelIndex &parent) const;
	int columnCount(const QModelIndex &parent) const;

signals:

public slots:
	void refresh();
	void refreshPen(const QString &name);

private:

private:
	int indexOfPen(const QString &name);
	QDir penDir(const QString &pen) const;
	QDir notebookDir(const QString &pen, const QString &notebook) const;
	QString penDisplayName(const QString &pen) const;
	QString notebookDisplayName(const QString &pen, const QString &notebook) const;
	QIcon penIcon(const QString &pen) const;
	QIcon notebookIcon(const QString &pen, const QString &notebook) const;
	bool isPenArchive(const QString &pen) const;
	bool isPenLocked(const QString &pen) const;
	bool isNotebookLocked(const QString &pen, const QString &notebook) const;
	bool isPaperReplayLocked(const QString &pen) const;

	int estimatePagesOfNotebook(const QString &pen, const QString &notebook) const;

private slots:
	void handleChangedDirectory(const QString &path);

private:
	QDir _dataDir;
	QFileSystemWatcher _watcher;
	QStringList _pens;
	QHash<QString, QStringList> _notebooks;

	mutable QHash<QString, QIcon> _iconCache;
};

#endif // NOTEBOOKMODEL_H
