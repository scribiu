/*
 * scribiu -- read notebooks and voice memos from Livescribe pens
 * Copyright (C) 2015 Javier S. Pedro <javier@javispedro.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AFDNOTEBOOK_H
#define AFDNOTEBOOK_H

#include <QtCore/QDateTime>
#include <QtCore/QDir>
#include <QtCore/QMap>
#include <QtGui/QImage>
#include "afdpageaddress.h"
#include "stfreader.h"

class AfdNotebook : public QObject
{
	Q_OBJECT

public:
	AfdNotebook(QObject *parent = 0);
	~AfdNotebook();

	bool open(const QString &path);
	void close();

	QString title() const;
	quint64 guid() const;

	int numPages() const;

	AfdPageAddress getPageAddress(int pageNum) const;
	int getPageNumber(const AfdPageAddress &addr) const;

	QString getPageBackgroundName(int page) const;
	QPixmap getPageBackground(int page);

	QSize getPageSize(int page) const;
	QRect getPageTrim(int page) const;

	QStringList penSerials() const;
	QList<int> pagesWithStrokes(const QString &penSerial) const;

	QStringList strokeFiles(const QString &penSerial, int page) const;

private:
	struct Gfx {
		QString basename;
	};

	struct Page {
		Gfx *gfx;
		QSize size;
	};

	struct StrokeData {
		QString file;
		quint64 begin;
		quint64 end;

		static bool CompareByBeginTime(const StrokeData &a, const StrokeData &b) {
			return a.begin < b.begin;
		}
	};

	struct PenData {
		// pageNum -> List of Strokes
		QMap<int, QList<StrokeData>> strokes;
	};

private:
	static QMap<QString, QString> parsePropertyList(QIODevice *dev);
	QMap<QString, QString> parsePropertyList(const QString &relativePath) const;

	bool parseMainInfo();
	bool parseMainDocument();
	bool parseGfx(const QString &file);
	bool findPenData();

private:
	QDir _dir;
	QString _title;
	quint64 _guid;
	AfdPageAddress _firstPage, _lastPage;
	uint _pagesPerBook;
	QMap<QString, Gfx> _gfx;
	QList<Page> _pages;
	QMap<QString, PenData> _penData;
};

#endif
