/*
 * scribiu -- read notebooks and voice memos from Livescribe pens
 * Copyright (C) 2015 Javier S. Pedro <javier@javispedro.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STFSTROKEITEM_H
#define STFSTROKEITEM_H

#include <QtWidgets/QGraphicsPathItem>
#include "paperreplay.h"

class StfStrokeItem : public QGraphicsPathItem
{
public:
	StfStrokeItem(const QPainterPath &stroke, const PaperReplay::Session &session, qint64 startTime, qint64 endTime, QGraphicsItem *parent = 0);

	enum { Type = UserType + 't' };

	int type() const override;

	bool hasPaperReplay() const;

protected:
	void mousePressEvent(QGraphicsSceneMouseEvent *event) override;

private:
	PaperReplay::Session _session;
	qint64 _startTime;
	qint64 _endTime;
};

#endif // STFSTROKEITEM_H
