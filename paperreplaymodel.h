/*
 * scribiu -- read notebooks and voice memos from Livescribe pens
 * Copyright (C) 2015 Javier S. Pedro <javier@javispedro.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAPERREPLAYMODEL_H
#define PAPERREPLAYMODEL_H

#include <QtCore/QAbstractTableModel>
#include "paperreplay.h"

class PaperReplayModel : public QAbstractTableModel
{
	Q_OBJECT
public:
	explicit PaperReplayModel(PaperReplay *replay, QObject *parent = 0);

	QVariant data(const QModelIndex &index, int role) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role) const;
	int rowCount(const QModelIndex &parent) const;
	int columnCount(const QModelIndex &parent) const;

	QString sessionFilename(const QModelIndex &index) const;

signals:

public slots:
	void refresh();

private:
	QString getSessionName(const PaperReplay::Session &session) const;
	QString getSessionLength(const PaperReplay::Session &session) const;

private:
	PaperReplay *_replay;
	QList<PaperReplay::Session> _sessions;
};

#endif // PAPERREPLAYMODEL_H
