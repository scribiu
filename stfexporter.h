/*
 * scribiu -- read notebooks and voice memos from Livescribe pens
 * Copyright (C) 2021 Javier S. Pedro <javier@javispedro.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STFEXPORTER_H
#define STFEXPORTER_H

#include "afdnotebook.h"

class StfExporter
{
public:
	explicit StfExporter(AfdNotebook *nb);

	void exportToTXYP(QIODevice *out, int pageNum, bool relativeTime);
	void exportToInkML(QIODevice *out, int pageNum);

private:
	bool exportPage(StfReader::StrokeHandler *handler, int pageNum);

private:
	AfdNotebook *_nb;
};

#endif // STFEXPORTER_H
