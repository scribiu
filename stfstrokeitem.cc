/*
 * scribiu -- read notebooks and voice memos from Livescribe pens
 * Copyright (C) 2015 Javier S. Pedro <javier@javispedro.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtCore/QDebug>
#include <QtGui/QPen>
#include <QtGui/QCursor>
#include <QtWidgets/QGraphicsScene>
#include <QtWidgets/QGraphicsSceneMouseEvent>
#include "notebookview.h"
#include "stfstrokeitem.h"

StfStrokeItem::StfStrokeItem(const QPainterPath &stroke, const PaperReplay::Session &session, qint64 startTime, qint64 endTime, QGraphicsItem *parent)
    : QGraphicsPathItem(stroke, parent), _session(session),
      _startTime(startTime), _endTime(endTime)
{
	QPen pen(Qt::black, 8.0f);
	if (hasPaperReplay()) {
		pen.setColor(Qt::darkGreen);
		setCursor(Qt::PointingHandCursor);
	}
	setPen(pen);
}

int StfStrokeItem::type() const
{
	return Type;
}

bool StfStrokeItem::hasPaperReplay() const
{
	return _session.isValid() && !_session.fileName().isEmpty();
}

void StfStrokeItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	if (event->button() == Qt::LeftButton && hasPaperReplay()) {
		QGraphicsView *view = scene()->views().first();
		if (NotebookView *nbview = qobject_cast<NotebookView*>(view)) {
			PaperReplay::PenTime time = _startTime - _session.startTime();
			if (time < 10) time = 0;

			qDebug() << "requesting paper replay at time" << time << "/" << _session.duration();

			nbview->requestPaperReplay(_session.fileName(), time);
			event->accept();
			return;
		}
	}
	QGraphicsItem::mousePressEvent(event);
}
